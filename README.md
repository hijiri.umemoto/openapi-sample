# openapi-sample

分割されているopenapiの定義ファイルをマージするコマンド

```bash
bash swagger-merger.sh
```

openapiの定義ファイルからクライアントコードを自動生成するコマンド

```bash
yarn openapi-generator-cli generate \
    -i /workspaces/docker/api-mock/openapi.yaml \
    -g typescript-axios \
    -o /workspaces/src/axios/api
```
