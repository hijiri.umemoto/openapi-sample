#!/bin/sh

base_filename="docker/api-mock/base.yaml"
out_filename="docker/api-mock/openapi.yaml"

yarn swagger-merger \
    -i $base_filename \
    -o $out_filename

while grep -q "\$ref: [^']*\.yaml" "$out_filename"
do
    yarn swagger-merger \
        -i $out_filename \
        -o $out_filename
done

echo "出力が完了しました"